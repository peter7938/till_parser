from till_app import app
from flask import jsonify, request, make_response
import pikepdf
import PyPDF2
from .utils import process_pdf, allowed_file


@app.route('/file-upload', methods=['POST'])
def file_upload():
    if 'file' not in request.files:
        return jsonify({'message':'No file part in the request'})
    file = request.files['file']
    password = request.form.get('password')
    if file.filename == '':
        resp = jsonify({'message' : 'No file selected for uploading'})
        resp.status_code = 400
        return resp
    if file and allowed_file(file.filename):
        #filename = secure_filename(file.filename)
        #get file name
        x = process_pdf(file, password)
        resp = jsonify({'message' : 'File successfully uploaded'})
        resp.status_code = 201
        return x
    else:
        resp = jsonify({'message' : 'Allowed file types are pdf'})
        resp.status_code = 400
        return resp
