import io
import re
import os
import pandas as pd
from io import StringIO, BytesIO
import pikepdf
import PyPDF2
from PyPDF2 import PdfFileReader
from openpyxl import Workbook
from openpyxl.styles import Font, Color, colors
import random, string
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl import load_workbook
from flask import make_response

regex = r'(\d{4}-\d{2}-\d{2} \d{2}\:\d{2}\:\d{2})(?<= \d{2}:\d{2}:\d{2})(\w{10})(.+?)(Completed)(.*?\.\d{2})(.*?\.\d{2})'


def allowed_file(filename):
    ALLOWED_EXTENSIONS = set(['pdf'])
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def extract_from_pdf(file, password):
    #decrypting the encrypted pdf file
    content = pikepdf.open(file, password)
    inmemory_file = BytesIO()
    content.save(inmemory_file)
    #reading and extracting data from the decrypted pdf file
    pdf_reader = PyPDF2.PdfFileReader(inmemory_file)
    num_pages = pdf_reader.getNumPages()
    extracted_data = StringIO()
    for page in range(num_pages):
        extracted_data.writelines(pdf_reader.getPage(page).extractText())

    return num_pages, extracted_data

def random_str(length=8):
	s = ''
	for i in range(length):
		s += random.choice(string.ascii_letters + string.digits)

	return s

def parse_mpesa_content(extracted_data):
    extracted_data.seek(0)
    lines = extracted_data.read()
    matches = re.compile(regex).findall(lines)

    fb = Font(name='Calibri', color=colors.BLACK, bold=True, size=11, underline='single')
    i = 0
    #creating the spreadsheet
    book = Workbook()
	# grab the active worksheet
    sheet = book.active
	#excel styling 2
    ft = Font(name='Calibri', color=colors.BLUE, bold=True, size=11, underline='single')

    sheet['A1'] = 'Transaction_date'
    sheet['B1'] = 'Receipt_no'
    sheet['C1'] = 'Transaction_details'
    sheet['D1'] = 'Transaction_status'
    sheet['E1'] = 'Transaction_amount'
    sheet['F1'] = 'Account_balance'

    a1 = sheet['A1']
    b1 = sheet['B1']
    c1 = sheet['C1']
    d1 = sheet['D1']
    e1 = sheet['E1']
    f1 = sheet['F1']

    a1.font = ft
    b1.font = ft
    c1.font = ft
    d1.font = ft
    e1.font = ft
    f1.font = ft


	#adding every match to the excel file
    while i < len(matches):
	    # print(matches[i])
        sheet.append(matches[i])
        i = i + 1

    filename = random_str()
    book.save(filename)
    f = open(filename, 'rb')
    file = BytesIO(f.read())
    f.close()
    os.remove(filename)

    return file


def output_json(workbook):
    excel_df = pd.read_excel(workbook)
    excel_df = excel_df[['Transaction_date', 'Receipt_no', 'Transaction_details', 'Transaction_amount', 'Account_balance']]
    excel_df['Transaction_amount'] = excel_df['Transaction_amount'].astype(str).str.replace(',', '').astype(float)
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Pay merchant Charge', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Organization Settlement Account to Charges', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Business Buy Goods Charge by Receiver', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Business To Business Transfer via API to', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Organization Settlement Account to Organization', regex=True)]


    def format1(row):
        index = None
        reg = re.search(r'([0-9]\d*[0-9])', row['Transaction_details'])
        if reg:
            index = reg.end()
            row['B_party'] = row['Transaction_details'][:index]

        return row

    def format2(row):
        index = None
        reg = re.search(r'([0-9]\d*[0-9])', row['B_party'])
        if reg:
            index = reg.start()
            row['B_party'] = row['B_party'][index:]

        return row

    def format3(row):
        match = re.findall(r'(\d+)', row['Transaction_details'])
        if len(match) > 1:
            row['B_party'] = match[-1]
        else:
            row['B_party'] = match[0]

        return row

    def withdrawal(excel_df):
        withdrawal = excel_df[excel_df['Transaction_amount']<0]
        withdrawal['Transaction_amount'] = withdrawal['Transaction_amount'].astype(str).str.replace('-', '')
        withdrawal['Transaction_type'] = 'Settlement'
        withdrawal['B_party'] = 'Null'
        return withdrawal

    withdrawn = withdrawal(excel_df)

    def paid_in(excel_df):
        paid = excel_df[excel_df['Transaction_amount']>0]
        paid['Transaction_type'] = 'Payment'
        paid['Transaction_amount'] = paid['Transaction_amount'].astype(str)
        #sorted_df1 = paid.apply(format1, axis=1)
        #paid = sorted_df1.apply(format2, axis=1)
        paid = paid.apply(format3, axis=1)

        return paid

    paid = paid_in(excel_df)

    return withdrawn, paid

def json(dataframe1, dataframe2):
    combined_json = dataframe1.append(dataframe2, ignore_index=True)
    d1_json = combined_json.to_json(orient='records')

    return d1_json


def process_pdf(file, password):
    error = ''
    try:
        numpages, txtfile = extract_from_pdf(file, password)
    except Exception:
        error = 'Check file input and password!'
    else:
        content = parse_mpesa_content(txtfile)
        if content:
            #content2 = exec_analytics(content)
            #pandas operation
            dataframe1, dataframe2 = output_json(content)
            dataframe_json = json(dataframe1, dataframe2)
            resp = make_response((dataframe_json, {
                'Content-Type': 'application/json',
                'Content-Disposition': 'attachment; filename=peter.json'
            }))
        else:
            resp =  make_response({'response' : content}, {
                'Content-Type': 'application/json',
            })
        return resp
